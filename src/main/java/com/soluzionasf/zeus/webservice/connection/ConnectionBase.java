package com.soluzionasf.zeus.webservice.connection;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionBase {

	private static String url;
	private static String user;
	private static String password;
	private static String driverName;
	public Connection con;

	
	public ConnectionBase() throws IOException, SQLException, ClassNotFoundException {
		Properties properties = new Properties();
		try {

			InputStream input = new FileInputStream("config/database.properties");
			properties.load(input);
			driverName = properties.getProperty("driverClassName");
			url = properties.getProperty("url");
			user = properties.getProperty("user");
			password = properties.getProperty("password");
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() throws SQLException, ClassNotFoundException {

		Connection con = null;
		try {
			Class.forName(driverName);
			con = DriverManager.getConnection(url, user, password);
			return con;
		} catch (SQLException e) {
			System.out.println("Falha ao conectar no banco de dados: " + e.getMessage()+"--\n"+e.getCause()+"--\n"+e.getSuppressed());
		}
		return con;
	}



	public static void closeConnection(Connection conn, PreparedStatement pstm, ResultSet rs) throws Exception {
		close(conn, pstm, rs);
	}

	public static void closeConnection(Connection conn, PreparedStatement pstm) throws Exception {
		close(conn, pstm, null);
	}

	public static void closeConnection(Connection conn) throws Exception {
		close(conn, null, null);
	}

	private static void close(Connection conn, PreparedStatement pstm, ResultSet rs) throws Exception {
		try {
			if (rs != null)
				rs.close();
			if (pstm != null)
				pstm.close();
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

}
