package com.soluzionasf.zeus.webservice.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.soluzionasf.zeus.webservice.bean.EventBean;
import com.soluzionasf.zeus.webservice.dao.EventsDAO;
import com.soluzionasf.zeus.webservice.statics.Configuration;

public class CheckerController {

	//private static Collection<EventBean> eventBeanList = new HashSet<EventBean>();
	private static Map<Integer, Integer> timerMap = new HashMap<Integer, Integer>();
	//private static Collection<EventBean> timerListXml = new HashSet<EventBean>();
	private static Document doc;
	private EventBean eventBean;
	public String driverName = null;
	
	static{
     	   File fileXml = new File("config/timers.xml");
	       DocumentBuilderFactory fileFactory = DocumentBuilderFactory.newInstance();
	       try {
	    	   DocumentBuilder dBuilder = fileFactory.newDocumentBuilder();
			   doc = dBuilder.parse(fileXml);
			   //getTimersMap();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor with one parameter
	 * @param pathService
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public CheckerController(EventBean eventBean) throws ParserConfigurationException, SAXException, IOException{
		  
		this.eventBean = eventBean;
		
	}
	/**
	 * Constructor without parameter
	 * @throws IOException 
	 * 
	 * 
	 */
	public CheckerController() throws IOException{
		
		Properties properties = new Properties();
		InputStream input = new FileInputStream("config/database.properties");
		properties.load(input);
		this.driverName = properties.getProperty("driverClassName");
		
	}
	
	/**
	 * This method save the eventObject in DataBase
	 * Obs.: service == id_timer (in database)
	 */
	public void save(EventBean eventBean){
		//System.out.println("--->"+eventBean.getServer());
		try {
			
				EventsDAO eventsDAO = new EventsDAO();
				//pega o time atual do evento
				String endTimeEvent = TimerUtils.getCurrentTimeEvent();
				//adiciona "SUCCESS"
				eventBean.setExecutionResult(Configuration.DAO_RESULT_EXECUTION_SUCCESS);
				
			    eventBean.setEndTimeExecution(endTimeEvent);
			    if(this.driverName.matches("oracle.jdbc.driver.OracleDriver")){
			    	eventsDAO.saveEventOracle(eventBean);
			    }else{
			    	eventsDAO.saveEvent(eventBean);
			    }
				
			
		} catch (Exception e) {
			System.out.println("Erro DAO: "+e.getMessage()+"-"+e.getCause());
			e.getStackTrace();
		}
		
		
		
	}
	
	/**
	 * This method save the eventObject in DataBase using a ERROR flag.
	 * This error occurs when some part of input is empty like startDate or endDate or serverName 
	 */
	public void saveEventFailed(EventBean eventBean, String error){
				
		try {
			EventsDAO eventsDAO = new EventsDAO();
			String endTimeEvent = TimerUtils.getCurrentTimeEvent();
			
			//int id = eventsDAO.checkTimerStatus(eventBean);
			eventBean.setExecutionResult(Configuration.DAO_RESULT_EXECUTION_FAIL);
			eventBean.setErrorMsg(error);
			eventBean.setEndTimeExecution(endTimeEvent);
			//eventsDAO.saveEventCloseIt(id, eventBean);		
			eventsDAO.saveEvent(eventBean);
		
	} catch (Exception e) {
		e.getStackTrace();
	}
		
		
	}
	
	
	/**
	 * This method return a object of main list. 
	 * @return
	 */
	public EventBean getEvent(){
		
		return new EventBean();
	}
	/**
	 * After finished the event, the died event object must be removed
	 * from the main list.
	 * @return 
	 */
	public boolean removeEvent(){
		
		return false;
	}
	/**
	 * This method return a map of timers and their values. 
	 * @return
	 */
	public static void getTimersMap(){
		
		 NodeList nListId    = doc.getElementsByTagName("id");
	     NodeList nListValue = doc.getElementsByTagName("value");
	     
	     for (int temp = 0; temp < nListId.getLength(); temp++) {
	    	   
             Node nNodeId    = nListId.item(temp);
             Node nNodeValue = nListValue.item(temp);
             
             if (nNodeId.getNodeType() == Node.ELEMENT_NODE) {
                  Element eElementId = (Element) nNodeId;
                  Element eElementValue = (Element) nNodeValue;
                  String id = eElementId.getTextContent();
                  String value = eElementValue.getTextContent();
                  timerMap.put(new Integer(id), new Integer(value));
             }
         }

		//System.out.println("Valor do MAP: "+timerMap.get(11));

		
	}
	
}
