package com.soluzionasf.zeus.webservice.bean;

import java.util.List;

/**
 * Bean created only for store each timer from timers.xml in a simple object;
 *  
 * @author tpereirac
 *
 */
public class TimerBean {

	private String group;               //ex.:   FieldService
	private int id;			            //ex.:   1
	private String project;		        //ex.:   TimersWS
	private String service;		        //ex.:   TimerFSScheduleBeanWS
	private String prefix;		        //ex.:   zeus
	private String uri;			        //ex.:   http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule
	private String method;		        //ex.:   findOutages
	private String name;			    //ex.:   TIME_TO_CHECK_ACTUATED_EVENT
	private String wsdl;				//ex.:   http://localhost:7001/TimersWS/TimerOPScheduleBeanWS?wsdl
	private List<String> headerList;	//ex.:   <headerElement>kkkkk</headerElement> <headerElement>kkkkk</headerElement>
	private List<String> bodyList;		//ex.:  <bodyElement>uc</bodyElement> <bodyElement>car</bodyElement>
	private String user;
	private String password;
	
	@Override
	public String toString() {
		return "TimerBean [group=" + group + ", id=" + id + ", project=" + project + ", service=" + service
				+ ", prefix=" + prefix + ", uri=" + uri + ", method=" + method + ", name=" + name + ", headerList="
				+ headerList + ", bodyList=" + bodyList + "]";
	}
	
	public String getUser() {
		return user;
	}



	public void setUser(String user) {
		this.user = user;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}

    
	public TimerBean(){
		
	}



	public String getGroup() {
		return group;
	}



	public void setGroup(String group) {
		this.group = group;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getProject() {
		return project;
	}



	public void setProject(String project) {
		this.project = project;
	}



	public String getService() {
		return service;
	}



	public void setService(String service) {
		this.service = service;
	}



	public String getPrefix() {
		return prefix;
	}



	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}



	public String getUri() {
		return uri;
	}



	public void setUri(String uri) {
		this.uri = uri;
	}



	public String getMethod() {
		return method;
	}



	public void setMethod(String method) {
		this.method = method;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public List<String> getHeaderList() {
		return headerList;
	}



	public void setHeaderList(List<String> headerList) {
		this.headerList = headerList;
	}



	public List<String> getBodyList() {
		return bodyList;
	}



	public void setBodyList(List<String> bodyList) {
		this.bodyList = bodyList;
	}



	public String getWsdl() {
		return wsdl;
	}



	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}
	
	
	
	
	
	
	
}
