package com.soluzionasf.zeus.webservice.dao;

import java.io.IOException;
import java.security.KeyStore.ProtectionParameter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.soluzionasf.zeus.webservice.bean.EventBean;
import com.soluzionasf.zeus.webservice.connection.ConnectionBase;

public class EventsDAO extends ConnectionBase implements IEvents{
	
	private Connection connection = null;
	
	public EventsDAO() throws IOException, SQLException, ClassNotFoundException{
		this.connection = ConnectionBase.getConnection();
	}
	
	/**
	 * This method save an event that just opened. 
	 */
	@Override
	public void saveEvent(EventBean eventObj) {
		try {
			String sql = "insert into events_timer ("
					+ "last_start_execution, server,"
					+ "last_result_execution, timer_name, error_msg, execution_interval_seconds, last_end_execution "
					+ ")values(?,?,?,?,?,?,?)";
			PreparedStatement pst = connection.prepareStatement(sql);
		    pst.setString(1 , eventObj.getCurrentTimeExecution());
			pst.setString(2 , eventObj.getServer());
			pst.setString(3 , eventObj.getExecutionResult());
			pst.setString(4 , eventObj.getTimerName());
			pst.setString(5 , eventObj.getErrorMsg());
			pst.setInt(6 , eventObj.getIntervalSeconds());
			pst.setString(7 , eventObj.getEndTimeExecution());
			//pst.setString(7 , eventObj.getTimerName());
			pst.executeUpdate();
			pst.close();
		} catch (SQLException e) {
			e.getSQLState();
			System.out.println("Error:"+e.getErrorCode()+"-"+e.getMessage());
		}
		
	}
	/**
	 * 
	 */
	@Override
	public void saveEventOracle(EventBean eventObj) {
		try {
			String sql = "insert into events_timer (event_id,"
					+ "last_start_execution, server,"
					+ "last_result_execution, timer_name, error_msg, execution_interval_seconds, last_end_execution "
					+ ")values(seq_events_timer.nextval,?,?,?,?,?,?,?)";
			PreparedStatement pst = connection.prepareStatement(sql);
			//pst.setInt(1 , "seq_events_timer.nextval");
			pst.setString(1 , eventObj.getCurrentTimeExecution());
			pst.setString(2 , eventObj.getServer());
			pst.setString(3 , eventObj.getExecutionResult());
			pst.setString(4 , eventObj.getTimerName());
			pst.setString(5 , eventObj.getErrorMsg());
			pst.setInt(6 , eventObj.getIntervalSeconds());
			pst.setString(7 , eventObj.getEndTimeExecution());
			pst.executeUpdate();
			pst.close();
		} catch (SQLException e) {
			e.getSQLState();
			System.out.println("Error:"+e.getErrorCode()+"-"+e.getMessage());
		}
		
		
	}
	
	/**
	 * This method save an event that just closed. 
	 */
	@Override
	public void saveEventCloseIt(int event_id, EventBean eventObj) {
		try {
			String sql = "update events_timer "
					+ "set last_end_execution = ?, last_result_execution = ? "
					+ "where event_id = ?";
			PreparedStatement pst = connection.prepareStatement(sql);
		    
			pst.setString(1 , eventObj.getCurrentTimeExecution());
			pst.setString(2 , eventObj.getExecutionResult());
			pst.setLong(3 , event_id);
			pst.executeUpdate();
			pst.close();
		} catch (SQLException e) {
			e.getSQLState();
			System.out.println("Error:.:"+e.getErrorCode()+"-"+e.getMessage());
		}
		
	}
	
	@Override
	public void deleteEvent() {
		
	}


	/**
	 * This method is able to check if a timer event has already created
	 * or already exists in database and is waiting for to be closed.
	 * 
	 * @return "-1" if the object there is not in the table;
	 * @Return  "id number" if there is an object in the table;
	 * 
	 */
	@Override
	public int checkTimerStatus(EventBean event) {
		int id_row = -1;
		try {
			PreparedStatement stmt = connection.prepareStatement("select event_id from events_timer "
					+ " where timer_name = ? AND last_result_execution IS NULL AND last_end_execution is null");
			
			//PreparedStatement pst = connection.prepareStatement(sql);
			 //pst.setString(1 , eventObj.getCurrentTimeExecution());
			stmt.setString(1 , event.getTimerName());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				id_row = rs.getInt("event_id");
			}
			//System.out.println("\nValue: "+ id_row);
		} catch (Exception e) {
			System.out.println("\nError SQL: "+e.getMessage()+"."+e.getCause());
		}
		return id_row;
	}
	
	@Override
	public List<EventBean> getEventList() {
		List<EventBean> eventList = new ArrayList<EventBean>();
		
		try {
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM EVENTS_TIMER");
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				EventBean eventBean = new EventBean();
				
				eventBean.setEventId(rs.getInt("event_id"));
				eventList.add(eventBean);
				
			}
		} catch (Exception e) {
			e.getStackTrace();
			System.out.println("Error:: "+e.getMessage());
		}
		
		
		return eventList;
	}




}
