
package com.indra.isd.integration.zeusservicesfstimerfieldserviceschedule;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.indra.isd.integration.zeusservicesfstimerfieldserviceschedule package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ExecuteWorkflowResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeWorkflowResponse");
    private final static QName _ExecuteSendOSToUE_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeSendOSToUE");
    private final static QName _ExecuteWorkflow_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeWorkflow");
    private final static QName _ExecuteRoute_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeRoute");
    private final static QName _ExecuteFullDataSyncResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeFullDataSyncResponse");
    private final static QName _User_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "user");
    private final static QName _ExecutePriorization_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executePriorization");
    private final static QName _ExecutePriorizationResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executePriorizationResponse");
    private final static QName _ExecuteJmsToPendingSOResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeJmsToPendingSOResponse");
    private final static QName _Password_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "password");
    private final static QName _ExecuteProposeResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeProposeResponse");
    private final static QName _ExecutePropose_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executePropose");
    private final static QName _ExecuteSendOSToUEResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeSendOSToUEResponse");
    private final static QName _ExecuteJmsToPendingSO_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeJmsToPendingSO");
    private final static QName _ExecuteRouteResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeRouteResponse");
    private final static QName _EventReturnTimerBean_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "eventReturnTimerBean");
    private final static QName _ExecuteFullDataSync_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", "executeFullDataSync");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.indra.isd.integration.zeusservicesfstimerfieldserviceschedule
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteRoute }
     * 
     */
    public ExecuteRoute createExecuteRoute() {
        return new ExecuteRoute();
    }

    /**
     * Create an instance of {@link ExecuteFullDataSyncResponse }
     * 
     */
    public ExecuteFullDataSyncResponse createExecuteFullDataSyncResponse() {
        return new ExecuteFullDataSyncResponse();
    }

    /**
     * Create an instance of {@link ExecuteSendOSToUE }
     * 
     */
    public ExecuteSendOSToUE createExecuteSendOSToUE() {
        return new ExecuteSendOSToUE();
    }

    /**
     * Create an instance of {@link ExecuteFullDataSync }
     * 
     */
    public ExecuteFullDataSync createExecuteFullDataSync() {
        return new ExecuteFullDataSync();
    }

    /**
     * Create an instance of {@link ExecutePriorizationResponse }
     * 
     */
    public ExecutePriorizationResponse createExecutePriorizationResponse() {
        return new ExecutePriorizationResponse();
    }

    /**
     * Create an instance of {@link ExecuteSendOSToUEResponse }
     * 
     */
    public ExecuteSendOSToUEResponse createExecuteSendOSToUEResponse() {
        return new ExecuteSendOSToUEResponse();
    }

    /**
     * Create an instance of {@link ExecuteWorkflowResponse }
     * 
     */
    public ExecuteWorkflowResponse createExecuteWorkflowResponse() {
        return new ExecuteWorkflowResponse();
    }

    /**
     * Create an instance of {@link ExecutePriorization }
     * 
     */
    public ExecutePriorization createExecutePriorization() {
        return new ExecutePriorization();
    }

    /**
     * Create an instance of {@link ExecuteRouteResponse }
     * 
     */
    public ExecuteRouteResponse createExecuteRouteResponse() {
        return new ExecuteRouteResponse();
    }

    /**
     * Create an instance of {@link ExecutePropose }
     * 
     */
    public ExecutePropose createExecutePropose() {
        return new ExecutePropose();
    }

    /**
     * Create an instance of {@link ExecuteJmsToPendingSOResponse }
     * 
     */
    public ExecuteJmsToPendingSOResponse createExecuteJmsToPendingSOResponse() {
        return new ExecuteJmsToPendingSOResponse();
    }

    /**
     * Create an instance of {@link ExecuteJmsToPendingSO }
     * 
     */
    public ExecuteJmsToPendingSO createExecuteJmsToPendingSO() {
        return new ExecuteJmsToPendingSO();
    }

    /**
     * Create an instance of {@link EventReturnTimerBean }
     * 
     */
    public EventReturnTimerBean createEventReturnTimerBean() {
        return new EventReturnTimerBean();
    }

    /**
     * Create an instance of {@link ExecuteProposeResponse }
     * 
     */
    public ExecuteProposeResponse createExecuteProposeResponse() {
        return new ExecuteProposeResponse();
    }

    /**
     * Create an instance of {@link ExecuteWorkflow }
     * 
     */
    public ExecuteWorkflow createExecuteWorkflow() {
        return new ExecuteWorkflow();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteWorkflowResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeWorkflowResponse")
    public JAXBElement<ExecuteWorkflowResponse> createExecuteWorkflowResponse(ExecuteWorkflowResponse value) {
        return new JAXBElement<ExecuteWorkflowResponse>(_ExecuteWorkflowResponse_QNAME, ExecuteWorkflowResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteSendOSToUE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeSendOSToUE")
    public JAXBElement<ExecuteSendOSToUE> createExecuteSendOSToUE(ExecuteSendOSToUE value) {
        return new JAXBElement<ExecuteSendOSToUE>(_ExecuteSendOSToUE_QNAME, ExecuteSendOSToUE.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteWorkflow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeWorkflow")
    public JAXBElement<ExecuteWorkflow> createExecuteWorkflow(ExecuteWorkflow value) {
        return new JAXBElement<ExecuteWorkflow>(_ExecuteWorkflow_QNAME, ExecuteWorkflow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteRoute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeRoute")
    public JAXBElement<ExecuteRoute> createExecuteRoute(ExecuteRoute value) {
        return new JAXBElement<ExecuteRoute>(_ExecuteRoute_QNAME, ExecuteRoute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteFullDataSyncResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeFullDataSyncResponse")
    public JAXBElement<ExecuteFullDataSyncResponse> createExecuteFullDataSyncResponse(ExecuteFullDataSyncResponse value) {
        return new JAXBElement<ExecuteFullDataSyncResponse>(_ExecuteFullDataSyncResponse_QNAME, ExecuteFullDataSyncResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "user")
    public JAXBElement<String> createUser(String value) {
        return new JAXBElement<String>(_User_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecutePriorization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executePriorization")
    public JAXBElement<ExecutePriorization> createExecutePriorization(ExecutePriorization value) {
        return new JAXBElement<ExecutePriorization>(_ExecutePriorization_QNAME, ExecutePriorization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecutePriorizationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executePriorizationResponse")
    public JAXBElement<ExecutePriorizationResponse> createExecutePriorizationResponse(ExecutePriorizationResponse value) {
        return new JAXBElement<ExecutePriorizationResponse>(_ExecutePriorizationResponse_QNAME, ExecutePriorizationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteJmsToPendingSOResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeJmsToPendingSOResponse")
    public JAXBElement<ExecuteJmsToPendingSOResponse> createExecuteJmsToPendingSOResponse(ExecuteJmsToPendingSOResponse value) {
        return new JAXBElement<ExecuteJmsToPendingSOResponse>(_ExecuteJmsToPendingSOResponse_QNAME, ExecuteJmsToPendingSOResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "password")
    public JAXBElement<String> createPassword(String value) {
        return new JAXBElement<String>(_Password_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteProposeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeProposeResponse")
    public JAXBElement<ExecuteProposeResponse> createExecuteProposeResponse(ExecuteProposeResponse value) {
        return new JAXBElement<ExecuteProposeResponse>(_ExecuteProposeResponse_QNAME, ExecuteProposeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecutePropose }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executePropose")
    public JAXBElement<ExecutePropose> createExecutePropose(ExecutePropose value) {
        return new JAXBElement<ExecutePropose>(_ExecutePropose_QNAME, ExecutePropose.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteSendOSToUEResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeSendOSToUEResponse")
    public JAXBElement<ExecuteSendOSToUEResponse> createExecuteSendOSToUEResponse(ExecuteSendOSToUEResponse value) {
        return new JAXBElement<ExecuteSendOSToUEResponse>(_ExecuteSendOSToUEResponse_QNAME, ExecuteSendOSToUEResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteJmsToPendingSO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeJmsToPendingSO")
    public JAXBElement<ExecuteJmsToPendingSO> createExecuteJmsToPendingSO(ExecuteJmsToPendingSO value) {
        return new JAXBElement<ExecuteJmsToPendingSO>(_ExecuteJmsToPendingSO_QNAME, ExecuteJmsToPendingSO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteRouteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeRouteResponse")
    public JAXBElement<ExecuteRouteResponse> createExecuteRouteResponse(ExecuteRouteResponse value) {
        return new JAXBElement<ExecuteRouteResponse>(_ExecuteRouteResponse_QNAME, ExecuteRouteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventReturnTimerBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "eventReturnTimerBean")
    public JAXBElement<EventReturnTimerBean> createEventReturnTimerBean(EventReturnTimerBean value) {
        return new JAXBElement<EventReturnTimerBean>(_EventReturnTimerBean_QNAME, EventReturnTimerBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteFullDataSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesFSTimerFieldServiceSchedule", name = "executeFullDataSync")
    public JAXBElement<ExecuteFullDataSync> createExecuteFullDataSync(ExecuteFullDataSync value) {
        return new JAXBElement<ExecuteFullDataSync>(_ExecuteFullDataSync_QNAME, ExecuteFullDataSync.class, null, value);
    }

}
