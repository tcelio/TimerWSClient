package com.soluzionasf.zeus.webservice.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.soluzionasf.zeus.webservice.bean.EventBean;
import com.soluzionasf.zeus.webservice.bean.TimerBean;
import com.soluzionasf.zeus.webservice.statics.Configuration;

public class TimerUtils {

	private static HashMap<String, String> timerMap = new HashMap<String, String>();
	private static List<TimerBean> timersBeanList  = new ArrayList<TimerBean>();
	private static Document doc;

	
	/**
	 * bloco de c�digo carregado antes de tudo e uma unica vez.
	 */
	static {
		File fileXml = new File("config/timers.xml");
		DocumentBuilderFactory fileFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = fileFactory.newDocumentBuilder();
			doc = dBuilder.parse(fileXml);
			feedAllTimers();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * metodo auxiliar do bloco de inicializa��o est�tico
	 * 
	 */
	public static void feedAllTimers() {

		NodeList nListId             = doc.getElementsByTagName("id");
		NodeList nListGroup          = doc.getElementsByTagName("group");
		NodeList nListWsdl           = doc.getElementsByTagName("wsdl");
		NodeList nListPrefix 		 = doc.getElementsByTagName("namespace_prefix");
		NodeList nListUri 			 = doc.getElementsByTagName("namespace_uri");
		NodeList nListMethod		 = doc.getElementsByTagName("method");
		NodeList nListName 			 = doc.getElementsByTagName("name");
		NodeList nListHeaderElements = doc.getElementsByTagName("headerElements");
		NodeList nListBodyElements 	 = doc.getElementsByTagName("bodyElements");

		for (int temp = 0; temp < nListId.getLength(); temp++) {

					TimerBean timerBean = new TimerBean();
					List<String> headerList = new ArrayList<String>();
					List<String> bodyList = new ArrayList<String>();
					
					Node nNodeId              = nListId.item(temp);
					Node nGroup 		  	  = nListGroup.item(temp);  
					Node nWsdl  			  = nListWsdl.item(temp);
					Node nPrefix 			  = nListPrefix.item(temp);
					Node nUri 				  = nListUri.item(temp);
					Node nMethod 			  = nListMethod.item(temp);
					Node nName 				  = nListName.item(temp);
					Node nHeaderElements      = nListHeaderElements.item(temp);
					Node nBodyElement  	      = nListBodyElements.item(temp);
					
					if (nNodeId.getNodeType() == Node.ELEMENT_NODE) {
						
						Element eElementNodeId          = (Element) nNodeId;
						Element eElementHeaderElements  = (Element) nHeaderElements;
						Element eElementBodyElements    = (Element) nBodyElement;
						Element eElementName            = (Element) nName;
						Element eElementGroup           = (Element) nGroup;
						Element eElementPrefix    		= (Element) nPrefix;
						Element eElementWsdl    		= (Element) nWsdl;
						Element eElementUri    			= (Element) nUri;
						Element eElementMethod    		= (Element) nMethod;
		
						NodeList headerElementList = eElementHeaderElements.getChildNodes();
						  for (int j = 0; j < headerElementList.getLength(); j++) {
					            Node childNode = headerElementList.item(j);
					            if ("headerElement".equals(childNode.getNodeName())) {
					                //System.out.println("adicionado:"+headerElementList.item(j).getTextContent().trim());
					            	headerList.add(headerElementList.item(j).getTextContent().trim());
					            }
					        }
						
						 NodeList bodyElementList =  eElementBodyElements.getChildNodes();
						 for (int j = 0; j < bodyElementList.getLength(); j++) {
					            Node childNode = bodyElementList.item(j);
					            if ("bodyElement".equals(childNode.getNodeName())) {
					           //    System.out.println("adicionado:"+bodyElementList.item(j).getTextContent().trim());
					            	bodyList.add(bodyElementList.item(j).getTextContent().trim());
					            }
					        }
						 //tratativa para remover eventuais lineFeeds
						 String strURI = eElementUri.getTextContent();
						 strURI = strURI.replaceAll("\r","");
						 strURI = strURI.replaceAll("\n","");
						 strURI = strURI.replaceAll("\t","");
						 
						 timerBean.setId(new Integer(eElementNodeId.getTextContent()));
						 timerBean.setGroup(eElementGroup.getTextContent());
						 timerBean.setPrefix(eElementPrefix.getTextContent());
						 timerBean.setUri(strURI);
						 timerBean.setMethod(eElementMethod.getTextContent());
						 timerBean.setName(eElementName.getTextContent());
						 timerBean.setHeaderList(headerList);
						 timerBean.setBodyList(bodyList);
						 timerBean.setWsdl(eElementWsdl.getTextContent());
						// System.out.println("bean adicionado: "+timerBean.toString()+"---\n");
						 timersBeanList.add(timerBean);
						//System.out.println("->" + eElementName.getTextContent() + "----"+eElementHeaderElements.getTextContent());
						//timerMap.put(name,namePath);
			}
		}

	}
	
	/**
	 * This method retrieve a TimerBean object.
	 * @param name
	 * @return
	 */
	public static TimerBean getTimerById(String name){

		TimerBean timerBean = null;
		try{
			for(TimerBean timer: timersBeanList){
				if(timer.getName().matches(name)){
					//System.out.println("O nome existe na lista!"+name);
					timerBean = timer;
				}
			}
		}catch(Exception e){
			System.out.println("error>>>"+e.getMessage());
		}
		
		return timerBean;
	}

	/**
	 * This method is important for check the number of parameters is ok!!
	 * @return true is ok!
	 * @return false is a bad event, because the number of parameters is not correct!!
	 */
	public static boolean checkParametersNumbers(TimerBean timer, int parameters){
		boolean check = true;
		try {
			int number = timer.getBodyList().size() + timer.getHeaderList().size(); //header + body parameters
			if(number != (parameters - 3)){
				check = false;
				System.out.println("H� algo errado aqui!"+number+"--"+(parameters - 3));
			}
			//System.out.println("Esta tudo ok!!!");
		} catch (Exception e) {
			System.out.println("Error:: "+e.getMessage()+"-"+e.getCause());
		}
		return check;
	}
//	/**
//	 * This method check for empty user/passwords or user/password exists.
//	 * If there is one element empty and other filled, the method return -1 (error)
//	 * @param timer
//	 * @return
//	 */
//	public static int checkPasswordLogin(TimerBean timer){
//		int check = -1;
//		try {
//			if(!timer.getUser().isEmpty() && !timer.getPassword().isEmpty()){
//				check = 1;
//			}else if(timer.getUser().isEmpty() && timer.getPassword().isEmpty()){
//				check = 0;
//			}
//		} catch (Exception e) {
//			e.getStackTrace();
//		}
//		return check;
//	}
//	
	
	/**
	 * This method retrieve number of header elements
	 * int the soap request
	 * @param name
	 * @return
	 */
	
	public static int getHeaderValuesNum(String name){
		int headerNum = 0;
		try {
			for(TimerBean timer: timersBeanList){
				if(timer.getName().matches(name)){
					headerNum = timer.getHeaderList().size();
				}
			}
		} catch (Exception e) {
			System.out.println("Error:>:"+e.getMessage()+"."+e.getCause());
		}
		return headerNum;
	}
	/**
	 * This method retrieve number of Body elements
	 * int the soap request
	 * @param name
	 * @return
	 */
	
	public static int getBodyValuesNum(String name){
		int bodyNum = 0;
		try {
			for(TimerBean timer: timersBeanList){
				if(timer.getName().matches(name)){
					bodyNum = timer.getBodyList().size();
				}
			}
		} catch (Exception e) {
			System.out.println("Error:>:"+e.getMessage()+"."+e.getCause());
		}
		return bodyNum;
	}
	
	/**
	 * Atraves da chave, conseguimos o valor referente,que � o Path.
	 * ex.:
	 * @param key
	 * @return
	 */
	public static String getValueFromHashMap(String key) {
		String valueName = "";
		Set<Entry<String, String>> entires = timerMap.entrySet();
		for (Entry<String, String> ent : entires) {
			if (ent.getKey().matches(key)) {
				valueName = ent.getValue();//ent.getKey()
			}
		}
		return valueName;
	}

	/**
	 * Atrav�s do value, voc� consegue ter acesso o valor da key deste timer.
	 * 
	 * @param id
	 * @return
	 */
	public static String getTimerById(int id) {
		String timer = null;
		try {
			timer = timerMap.get(id);
		} catch (Exception e) {
			e.getStackTrace();
		}

		return timer;
	}

	/**
	 * Aqui voc� recupera o nome do timer se tiver o ID dele na tabela de banco
	 * dela.
	 * 
	 * @param hm
	 * @param timer
	 * @return
	 */
	public Object getIdByTimerName(Map hm, String timer) {
		for (Object o : hm.keySet()) {
			if (hm.get(o).equals(timer)) {
				return o;
			}
		}
		return null;
	}
	/**
	 * 
	 * @param name
	 * @param parameters 
	 * @return
	 */
	public static List<String> getBodyList(String name, String[] parameters) {
		List<String> bodyNum = new ArrayList<String>();
		try {
			
			for(TimerBean timer: timersBeanList){
				if(timer.getName().matches(name)){
					 int header = timer.getHeaderList().size();
					 for(int b = (3+header); b <= parameters.length-1; b++){
						 bodyNum.add(parameters[b]);
					 }
				}
			}
		} catch (Exception e) {
			System.out.println("Error:::"+e.getMessage()+".|."+e.getCause());
		}
		
		return bodyNum;
	}

	/**
	 * 
	 * @param name
	 * @param parameters 
	 * @return
	 */
	public static List<String> getHeadList(String name, String[] parameters) {
		List<String> headNum = new ArrayList<String>();
		try {
			
			for(TimerBean timer: timersBeanList){
				if(timer.getName().matches(name)){
					 int body = timer.getBodyList().size();
					 for(int b = 3; b <= (parameters.length - body - 1); b++){
						 headNum.add(parameters[b]);
					 }
				}
			}
		} catch (Exception e) {
			System.out.println("Error:::"+e.getMessage()+".|."+e.getCause());
		}
		
		return headNum;
	}
	/**
	 * This method was created for make the wsdl url using: 
	 *   "http://"   +   server   + "/"  + project   + "/"  + service  +  "?wsdl" 
	 * @param timerBean
	 * @param eventBean
	 * @return
	 */
	public static String createWsdl(TimerBean timerBean, EventBean eventBean) {

		String wsdl = null;
		try {
			String wsdlStr = timerBean.getWsdl();
			String server  =  eventBean.getServer();
			if(wsdlStr.contains("//")){
				int position = wsdlStr.indexOf("//");
				wsdlStr = wsdlStr.substring(position+2, wsdlStr.length());
			}
			int position_ = wsdlStr.indexOf("/");
			wsdl = "http://"+server+"/"+wsdlStr.substring(position_+1, wsdlStr.length());
		} catch (Exception e) {
			e.getStackTrace();
		}
		return wsdl;
	}
	
	public static URL createWsdlURL(TimerBean timerBean, EventBean eventBean, int timeSeconds) {
		String wsdlStr = timerBean.getWsdl();
		String server  =  checkURL(eventBean.getServer());
		final int timeSeconds_ = timeSeconds;
		URL endpoint = null;
		try {
			endpoint =
					  new URL(new URL(server),
							  wsdlStr,
					          new URLStreamHandler() {
					            @Override
					            protected URLConnection openConnection(URL url) throws IOException {
					              URL target = new URL(url.toString());
					              URLConnection connection = target.openConnection();
					              // Connection settings milliseconds
					              connection.setConnectTimeout(5000); // time for create the connection.
					              connection.setReadTimeout(timeSeconds_*1000); //*1000 = x * 1 second time for finish the event while is connect with server.
					              return(connection);
					            }
					          });
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return endpoint;
	}

	
	public static String checkURL(String url){
		String url_ = null;
		try {
				if(!url.startsWith("http://") || !url.startsWith("https://")){
					url_ = "http://"+url;
				}else{
					url_ = url;
				}
			
		} catch (Exception e) {
			
		}
		return url_;
	}
	
	
	/**
	 * This method get the current date time from you current server and 
	 * will be saved in database if the time event work correctly.
	 * @return
	 */
	public static String getCurrentTimeEvent() {
		String currentTime = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(Configuration.TIME_STANDARD);
			Date date = new Date();
			currentTime = dateFormat.format(date).toString();
//			System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
		} catch (Exception e) {
			e.getStackTrace();
		}
		return currentTime;
	}


//	public static void main(String[] args){
//		System.out.println("foi");
//		System.out.println("Tamanho lista: "+timersBeanList.size());
//		int f = timersBeanList.size();
//		for(int h = 0; h <= f - 1; h++){
//			System.out.println("->"+timersBeanList.get(h).getHeaderList().get(1));
//		}
//		//feedAllTimers();
//	}
}
