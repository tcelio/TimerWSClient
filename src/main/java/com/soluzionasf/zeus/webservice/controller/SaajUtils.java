package com.soluzionasf.zeus.webservice.controller;

import java.io.IOException;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.BindingProvider;

import com.soluzionasf.zeus.webservice.bean.EventBean;
import com.soluzionasf.zeus.webservice.bean.TimerBean;
import com.soluzionasf.zeus.webservice.statics.Configuration;

public class SaajUtils {

	
	public static SOAPMessage createSoapRequest(TimerBean timerBean, EventBean eventBean){
		
		//eventBean parameters
		String prefix  = timerBean.getPrefix();
		String namespaceUri = timerBean.getUri();
		String method  =  timerBean.getMethod();
		
		//timerBean parameters
		List<String> headerList = timerBean.getHeaderList();
		List<String> bodyList = timerBean.getBodyList();
		
		SOAPMessage soapMessage = null;
		try {

			//-----item abaixo substituiu o de cima
			 MessageFactory messageFactory = MessageFactory.newInstance(Configuration.SOAP_VERSION);
			  soapMessage = messageFactory.createMessage();
			 SOAPPart soapPart = soapMessage.getSOAPPart();
	   	     SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
	  	     soapEnvelope.addNamespaceDeclaration(prefix, namespaceUri);//adicionado informacao

	  	     //Header - Verifica se existem elementos HEADER e consequentemente o HEADER.
	  	     if(headerList.size() > 0){
		  	    	SOAPHeader soapHeader = soapEnvelope.getHeader();
		  	    	//monta um a um os elementos existentes de header (prefixo + parametros + valor)
		  	   		for(int num = 0; num <= headerList.size() - 1; num++){	
		  	    		 SOAPElement headerElement = soapHeader.addChildElement(headerList.get(num),prefix); //adicionado informacao
		  	    		headerElement.addTextNode(eventBean.getHeaderList().get(num));//adicionado informacoes
		  	    	}
	  	     }
	  	   
	  	     
	  	     //Adiciona o elemento obrigatorio Body e seu metodo principal
	  	     SOAPBody soapBody = soapEnvelope.getBody();
	  	     SOAPElement soapBody_ =  soapBody.addChildElement(method, prefix); //adicionado informacao
	  	     	  	     
	  	     //Body - Verifica se existe elementos BODY ou se � vazio apenas tendo o method
	  	     if(bodyList.size() > 0){
		  	    	//monta um a um os elementos existentes de body (prefixo + parametros + valor)
		  	    	 for(int bodyNum = 0; bodyNum <= bodyList.size() -1; bodyNum++){
		  	    		 //SOAPElement soapBody_ =  soapBody.addChildElement("findOutages", "zeus");
		  	    		//System.out.println("-_-"+bodyList.get(bodyNum));
		  				 SOAPElement soapBody_2 = soapBody_.addChildElement(bodyList.get(bodyNum),prefix);
		  				 soapBody_2.addTextNode(eventBean.getBodyList().get(bodyNum));
		  	    	 }
	  	     }
        	 soapMessage.saveChanges();
			 System.out.println("----------SOAP Request------------");
			 soapMessage.writeTo(System.out);
			
		} catch (Exception e) {
			System.out.println("Error:::"+e.getMessage()+"..."+e.getCause());
		}
		return soapMessage;

	}

	public static SOAPMessage createSoapRequestSoapTwo(TimerBean timerBean, EventBean eventBean) throws SOAPException, IOException{
		//eventBean parameters
		String prefix  = timerBean.getPrefix();
		String namespaceUri = timerBean.getUri();
		String method  =  timerBean.getMethod();
		
		//timerBean parameters
		List<String> headerList = timerBean.getHeaderList();
		List<String> bodyList = timerBean.getBodyList();
		
		SOAPMessage soapMessage = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createMessage();
        soapMessage.getSOAPPart().getEnvelope().setPrefix("soap");
        soapMessage.getSOAPPart().getEnvelope().removeNamespaceDeclaration("env");
        soapMessage.getSOAPHeader().setPrefix("soap");
        soapMessage.getSOAPBody().setPrefix("soap");
        //SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
                SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
                soapEnvelope.addNamespaceDeclaration(prefix, namespaceUri);
                
        SOAPHeader soapHeader = soapEnvelope.getHeader();
        SOAPElement soapElement = soapHeader.addChildElement(method, prefix);
        if(headerList.size() > 0){
        	   soapHeader = soapEnvelope.getHeader();
	  	    	//monta um a um os elementos existentes de header (prefixo + parametros + valor)
	  	   		for(int num = 0; num <= headerList.size() - 1; num++){	
	  	    		 SOAPElement headerElement = soapHeader.addChildElement(headerList.get(num),prefix); //adicionado informacao
	  	    		headerElement.addTextNode(eventBean.getHeaderList().get(num));//adicionado informacoes
	  	    	}
        }
        
        //Adiciona o elemento obrigatorio Body e seu metodo principal
 	     SOAPBody soapBody = soapEnvelope.getBody();
        
        if(bodyList.size() > 0){
        	//monta um a um os elementos existentes de body (prefixo + parametros + valor)
 	    	 for(int bodyNum = 0; bodyNum <= bodyList.size() -1; bodyNum++){
 				 SOAPElement soapBody_ = soapBody.addChildElement(bodyList.get(bodyNum),prefix);
 				 soapBody_.addTextNode(eventBean.getBodyList().get(bodyNum));
 	    	 }
        	
        }
   
        soapMessage.saveChanges();
        System.out.println("----------SOAP Request------------");
        soapMessage.writeTo(System.out);
        return soapMessage;
	}

	public static void createSoapResponse(SOAPMessage soapResponse) throws TransformerConfigurationException, SOAPException {
		//EventBean eventBean;
		try {
			
			
			//System.out.println("Resultado do request: --::>"+soapResponse.getSOAPPart().getEnvelope().getBody().getFault().getFaultCode());
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			Source sourceContent = soapResponse.getSOAPPart().getContent();
			System.out.println("\n----------SOAP Response-----------");
			StreamResult result = new StreamResult(System.out);
			
			transformer.transform(sourceContent, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}
