
package com.indra.isd.integration.zeusservicesoptimeroperationschedule;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cfgTimePriorizationFSResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cfgTimePriorizationFSResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule}eventReturnTimerBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cfgTimePriorizationFSResponse", propOrder = {
    "_return"
})
public class CfgTimePriorizationFSResponse {

    @XmlElement(name = "return")
    protected EventReturnTimerBean _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link EventReturnTimerBean }
     *     
     */
    public EventReturnTimerBean getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventReturnTimerBean }
     *     
     */
    public void setReturn(EventReturnTimerBean value) {
        this._return = value;
    }

}
