package com.soluzionasf.zeus.webservice.bean;

import java.util.List;

public class EventBean {

	
	private int eventId;                    // ex.: numero aleatorio 1 2 3 4 etc
	private String timerName;				// ex.: nome do timer: TIME_TO_CHECK_ACTUATED_EVENT
	private String currentTimeExecution;    // ex.: 12:30:56 12/12/2012
	private String endTimeExecution;		// ex.: 12:32:56 12/12/2012
	private String executionResult;         // ex.:SUCCESS / ERROR
//	private String user;				    // fulano
	//private String password;			    // ex.: 123
	private String server;					// ex.: localhost:7000
	private String errorMsg;
	private int intervalSeconds;
	private List<String> headerList;        //values of the header elements of HEADER tag WSDL; the correct sequence is from timers.xml
	private List<String> bodyList;          //values of the body  elements of BODY tag WSDL; the correct sequence is from timers.xml
	

	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public String getTimerName() {
		return timerName;
	}
	public void setTimerName(String timerName) {
		this.timerName = timerName;
	}
	public String getCurrentTimeExecution() {
		return currentTimeExecution;
	}
	public void setCurrentTimeExecution(String currentTimeExecution) {
		this.currentTimeExecution = currentTimeExecution;
	}

	public String getExecutionResult() {
		return executionResult;
	}
	public void setExecutionResult(String executionResult) {
		this.executionResult = executionResult;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	
	
	
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public int getIntervalSeconds() {
		return intervalSeconds;
	}
	public void setIntervalSeconds(int intervalSeconds) {
		this.intervalSeconds = intervalSeconds;
	}
	public List<String> getHeaderList() {
		return headerList;
	}
	public void setHeaderList(List<String> headerList) {
		this.headerList = headerList;
	}
	public List<String> getBodyList() {
		return bodyList;
	}
	public void setBodyList(List<String> bodyList) {
		this.bodyList = bodyList;
	}
	public String getEndTimeExecution() {
		return endTimeExecution;
	}
	public void setEndTimeExecution(String endTimeExecution) {
		this.endTimeExecution = endTimeExecution;
	}

	
	
	
	
}
