package com.soluzionasf.zeus.webservice.dao;

import java.util.List;

import com.soluzionasf.zeus.webservice.bean.EventBean;

public interface IEvents {

	
	public void saveEvent(EventBean event);
	public void saveEventOracle(EventBean event);
	public void saveEventCloseIt(int event_id, EventBean event);
	public void deleteEvent();
	public List<EventBean> getEventList();
	public int checkTimerStatus(EventBean event);
	
	
	
}
