package com.soluzionasf.zeus.webservice.main;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerConfigurationException;

import com.soluzionasf.zeus.webservice.bean.EventBean;
import com.soluzionasf.zeus.webservice.bean.TimerBean;
import com.soluzionasf.zeus.webservice.controller.CheckerController;
import com.soluzionasf.zeus.webservice.controller.SaajUtils;
import com.soluzionasf.zeus.webservice.controller.TimerUtils;
import com.soluzionasf.zeus.webservice.statics.Configuration;
/**
 * Classe criada para ser a classe de execucao da aplicacao, que recebe os parametros da aplicacao. 
 * 
 * @author tpereirac
 * @version 1.0
 * @since 11/2016.
 *
 */
public class Start extends Thread{
	
	public Start(){

	}

	
	public static void main(String[] args) throws TransformerConfigurationException, IOException {

		int timeSecondsFull = -1;
		//String timeSecondsFullstr = null; //"60" = 1 minuto!
		
		String parameters[] = new String[args.length];
		for(int y = 0 ; y <= args.length - 1; y++){
			parameters[y] = args[y];
		}
		String name = null, server = null, timerSeconds = null;
		if(args.length >= 3){
			 server 		= parameters[0];  //ex: localhost:7001 ou 192.168.12.1200
			 name 		    = parameters[1];  //CFG_TIEMPO_EJECUCION_WORKFLOW ou ENABLE_CHECKS_SERVICE
			 timerSeconds   = parameters[2];  //ex: 900 seconds
			 if(timerSeconds != null && timerSeconds != ""){
				 timeSecondsFull = Integer.parseInt(timerSeconds);// adicionado 5 segundos de tolerancia.
				// timeSecondsFullstr = String.valueOf(timeSecondsFull);
			 }
			 		 
		}else{
			errorTimerParameters();
		}	
		//verifica se o timer existe no XML
		TimerBean timerBean = TimerUtils.getTimerById(name);

		//verifica se o numero de parametros corresponde ao numero de parametros inseridos
		boolean verify = TimerUtils.checkParametersNumbers(timerBean, parameters.length);
		
		EventBean eventBean = new EventBean();
		
		//se o timer existe no XML e os parametros est�o ok entra aqui!
		if((timerBean != null) && verify){
				//pega lista de parametros inseridos relacionado a body
				List<String> bodyList = TimerUtils.getBodyList(name, parameters);
				//pega lista de parametros inseridos relacionado a head
				List<String> headList = TimerUtils.getHeadList(name, parameters);
				//pega a hora atual do evento. Esta informa��o vai servir de inicio ou fim do evento do timer.
				String currentTimeEvent = TimerUtils.getCurrentTimeEvent();
				
				//alimenta o objeto com os dados passado por par�metro
				eventBean.setTimerName(name);
				eventBean.setServer(server);
				eventBean.setIntervalSeconds(new Integer(timerSeconds));
				eventBean.setBodyList(bodyList);
				eventBean.setHeaderList(headList);
				eventBean.setCurrentTimeExecution(currentTimeEvent);
			
		}else{
				errorTimerParameters();
		}

		SOAPMessage soapResponse = null;
		try {
			
				//cria uma instancia de conexao
			//ServiceClientFactory  = null;
				SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
				//monta uma conexao como se fosse um lego
				SOAPConnection soapConnection = soapConnectionFactory.createConnection();
				
				SOAPMessage soapRequest = null;
				if(Configuration.SOAP_VERSION.matches("1.2")){
					//cria um objeto (String) do tipo soapMessage
					soapRequest = SaajUtils.createSoapRequestSoapTwo(timerBean, eventBean);
				}else{
					//cria um objeto (String) do tipo soapMessage
					soapRequest = SaajUtils.createSoapRequest(timerBean, eventBean);
					
				}
								
				URL wsdl = TimerUtils.createWsdlURL(timerBean, eventBean, timeSecondsFull);
				
				//realiza efetivamente a chamada SOAP
				soapResponse = soapConnection.call(soapRequest,wsdl);
				
						/**
						 * tentativa de salvar evento no banco de dados foi colocado nesta posi��o devido a 
						 * necessidade de salvar o evento antes que ele acabe. Alem de ter casos de eventos
						 *  de webservices que demoram para terminar e pode afetar o fluxo da aplica��o.
						 */
						CheckerController checkerController = new CheckerController();
						
						try {
							
								checkerController.save(eventBean);
						}catch (Exception e) {
							e.getStackTrace();	
						}
				
				//solicita o tipo de response que deseja e obtem a String referente a solicitacao em g
				SaajUtils.createSoapResponse(soapResponse);
				//fecha o evento de conexao
				soapConnection.close();
		}catch(SOAPException e){
			
				CheckerController checkerController = new CheckerController();
				String error = e.getMessage();
				checkerController.saveEventFailed(eventBean, error);
				
				//System.out.println("\n Error: "+e.getMessage()+"--"+e.getCause()+"---"+e.getLocalizedMessage());
				//System.out.println("\nPassei aqui .. :"+soapResponse+" .");
		} 
		
			
	}

	public static void error(){
		System.out.println("ERROR: User and Password must be filled or both empty!");
		System.exit(0);
	}

	public static void errorTimerParameters(){
		System.out.println("ERROR: Number of parameters has error or the Timer is not in the timer list XML!");
		System.exit(0);
	}
}
