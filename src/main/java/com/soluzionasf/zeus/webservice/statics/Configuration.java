package com.soluzionasf.zeus.webservice.statics;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPConstants;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Configuration {

	
	private static Document docConf;
	
	
	public static String SOAP_VERSION;
	
	
	public static String DAO_RESULT_EXECUTION_SUCCESS;
	
	
	public static String DAO_RESULT_EXECUTION_FAIL;
	
	
	public static String TIME_STANDARD;
	
	
	
	
	
	static {
		File fileXmlConf = new File("config/configuration.xml");
		DocumentBuilderFactory fileFactoryConf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilderConf = fileFactoryConf.newDocumentBuilder();
			docConf = dBuilderConf.parse(fileXmlConf);
			feedConfiguration();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public static void feedConfiguration(){
		
		try {
			NodeList nodeSoapVersion = docConf.getElementsByTagName("SOAP_VERSION");
			NodeList nodeDaoSuccess = docConf.getElementsByTagName("DAO_RESULT_EXECUTION_SUCCESS");
			NodeList nodeDaoFail = docConf.getElementsByTagName("DAO_RESULT_EXECUTION_FAIL");
			NodeList nodeTime = docConf.getElementsByTagName("TIME_STANDARD");
			
			DAO_RESULT_EXECUTION_SUCCESS = nodeDaoSuccess.item(0).getTextContent();
			DAO_RESULT_EXECUTION_FAIL = nodeDaoFail.item(0).getTextContent();
			TIME_STANDARD = nodeTime.item(0).getTextContent();
			
			
			String soap_version_ = nodeSoapVersion.item(0).getTextContent();
			if(soap_version_.matches("1.2")){
				SOAP_VERSION = SOAPConstants.SOAP_1_2_PROTOCOL;
			}else{
				SOAP_VERSION = SOAPConstants.SOAP_1_1_PROTOCOL;
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}
	

}
