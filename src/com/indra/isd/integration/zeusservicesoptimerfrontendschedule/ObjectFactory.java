
package com.indra.isd.integration.zeusservicesoptimerfrontendschedule;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.indra.isd.integration.zeusservicesoptimerfrontendschedule package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReprocessInconsistency_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", "reprocessInconsistency");
    private final static QName _EventReturnTimerBean_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", "eventReturnTimerBean");
    private final static QName _ReprocessInconsistencyResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", "reprocessInconsistencyResponse");
    private final static QName _Password_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", "password");
    private final static QName _User_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", "user");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.indra.isd.integration.zeusservicesoptimerfrontendschedule
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReprocessInconsistencyResponse }
     * 
     */
    public ReprocessInconsistencyResponse createReprocessInconsistencyResponse() {
        return new ReprocessInconsistencyResponse();
    }

    /**
     * Create an instance of {@link EventReturnTimerBean }
     * 
     */
    public EventReturnTimerBean createEventReturnTimerBean() {
        return new EventReturnTimerBean();
    }

    /**
     * Create an instance of {@link ReprocessInconsistency }
     * 
     */
    public ReprocessInconsistency createReprocessInconsistency() {
        return new ReprocessInconsistency();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReprocessInconsistency }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", name = "reprocessInconsistency")
    public JAXBElement<ReprocessInconsistency> createReprocessInconsistency(ReprocessInconsistency value) {
        return new JAXBElement<ReprocessInconsistency>(_ReprocessInconsistency_QNAME, ReprocessInconsistency.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventReturnTimerBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", name = "eventReturnTimerBean")
    public JAXBElement<EventReturnTimerBean> createEventReturnTimerBean(EventReturnTimerBean value) {
        return new JAXBElement<EventReturnTimerBean>(_EventReturnTimerBean_QNAME, EventReturnTimerBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReprocessInconsistencyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", name = "reprocessInconsistencyResponse")
    public JAXBElement<ReprocessInconsistencyResponse> createReprocessInconsistencyResponse(ReprocessInconsistencyResponse value) {
        return new JAXBElement<ReprocessInconsistencyResponse>(_ReprocessInconsistencyResponse_QNAME, ReprocessInconsistencyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", name = "password")
    public JAXBElement<String> createPassword(String value) {
        return new JAXBElement<String>(_Password_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerFrontEndSchedule", name = "user")
    public JAXBElement<String> createUser(String value) {
        return new JAXBElement<String>(_User_QNAME, String.class, null, value);
    }

}
