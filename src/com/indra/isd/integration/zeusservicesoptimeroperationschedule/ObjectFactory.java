
package com.indra.isd.integration.zeusservicesoptimeroperationschedule;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.indra.isd.integration.zeusservicesoptimeroperationschedule package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TimeToCheckActuatedEventResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "timeToCheckActuatedEventResponse");
    private final static QName _ScadaAutomaticCloseALerts_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "scadaAutomaticCloseALerts");
    private final static QName _EnableChecksService_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "enableChecksService");
    private final static QName _QueueSerAutomaticCloseAlerts_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "queueSerAutomaticCloseAlerts");
    private final static QName _TimeToCheckResourceInativeResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "timeToCheckResourceInativeResponse");
    private final static QName _CfgChiEventTimeResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "cfgChiEventTimeResponse");
    private final static QName _User_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "user");
    private final static QName _CfgChiEventTime_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "cfgChiEventTime");
    private final static QName _ScadaAutomaticCloseALertsResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "scadaAutomaticCloseALertsResponse");
    private final static QName _QueueSerAutomaticCloseAlertsResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "queueSerAutomaticCloseAlertsResponse");
    private final static QName _CfgTimePriorizationFSResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "cfgTimePriorizationFSResponse");
    private final static QName _CfgTimePriorizationFS_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "cfgTimePriorizationFS");
    private final static QName _EnableChecksServiceResponse_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "enableChecksServiceResponse");
    private final static QName _EventReturnTimerBean_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "eventReturnTimerBean");
    private final static QName _TimeToCheckActuatedEvent_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "timeToCheckActuatedEvent");
    private final static QName _TimeToCheckResourceInative_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "timeToCheckResourceInative");
    private final static QName _Password_QNAME = new QName("http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", "password");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.indra.isd.integration.zeusservicesoptimeroperationschedule
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueueSerAutomaticCloseAlertsResponse }
     * 
     */
    public QueueSerAutomaticCloseAlertsResponse createQueueSerAutomaticCloseAlertsResponse() {
        return new QueueSerAutomaticCloseAlertsResponse();
    }

    /**
     * Create an instance of {@link CfgTimePriorizationFSResponse }
     * 
     */
    public CfgTimePriorizationFSResponse createCfgTimePriorizationFSResponse() {
        return new CfgTimePriorizationFSResponse();
    }

    /**
     * Create an instance of {@link TimeToCheckResourceInativeResponse }
     * 
     */
    public TimeToCheckResourceInativeResponse createTimeToCheckResourceInativeResponse() {
        return new TimeToCheckResourceInativeResponse();
    }

    /**
     * Create an instance of {@link EventReturnTimerBean }
     * 
     */
    public EventReturnTimerBean createEventReturnTimerBean() {
        return new EventReturnTimerBean();
    }

    /**
     * Create an instance of {@link QueueSerAutomaticCloseAlerts }
     * 
     */
    public QueueSerAutomaticCloseAlerts createQueueSerAutomaticCloseAlerts() {
        return new QueueSerAutomaticCloseAlerts();
    }

    /**
     * Create an instance of {@link CfgChiEventTime }
     * 
     */
    public CfgChiEventTime createCfgChiEventTime() {
        return new CfgChiEventTime();
    }

    /**
     * Create an instance of {@link EnableChecksServiceResponse }
     * 
     */
    public EnableChecksServiceResponse createEnableChecksServiceResponse() {
        return new EnableChecksServiceResponse();
    }

    /**
     * Create an instance of {@link ScadaAutomaticCloseALerts }
     * 
     */
    public ScadaAutomaticCloseALerts createScadaAutomaticCloseALerts() {
        return new ScadaAutomaticCloseALerts();
    }

    /**
     * Create an instance of {@link EnableChecksService }
     * 
     */
    public EnableChecksService createEnableChecksService() {
        return new EnableChecksService();
    }

    /**
     * Create an instance of {@link CfgTimePriorizationFS }
     * 
     */
    public CfgTimePriorizationFS createCfgTimePriorizationFS() {
        return new CfgTimePriorizationFS();
    }

    /**
     * Create an instance of {@link TimeToCheckResourceInative }
     * 
     */
    public TimeToCheckResourceInative createTimeToCheckResourceInative() {
        return new TimeToCheckResourceInative();
    }

    /**
     * Create an instance of {@link TimeToCheckActuatedEventResponse }
     * 
     */
    public TimeToCheckActuatedEventResponse createTimeToCheckActuatedEventResponse() {
        return new TimeToCheckActuatedEventResponse();
    }

    /**
     * Create an instance of {@link ScadaAutomaticCloseALertsResponse }
     * 
     */
    public ScadaAutomaticCloseALertsResponse createScadaAutomaticCloseALertsResponse() {
        return new ScadaAutomaticCloseALertsResponse();
    }

    /**
     * Create an instance of {@link CfgChiEventTimeResponse }
     * 
     */
    public CfgChiEventTimeResponse createCfgChiEventTimeResponse() {
        return new CfgChiEventTimeResponse();
    }

    /**
     * Create an instance of {@link TimeToCheckActuatedEvent }
     * 
     */
    public TimeToCheckActuatedEvent createTimeToCheckActuatedEvent() {
        return new TimeToCheckActuatedEvent();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimeToCheckActuatedEventResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "timeToCheckActuatedEventResponse")
    public JAXBElement<TimeToCheckActuatedEventResponse> createTimeToCheckActuatedEventResponse(TimeToCheckActuatedEventResponse value) {
        return new JAXBElement<TimeToCheckActuatedEventResponse>(_TimeToCheckActuatedEventResponse_QNAME, TimeToCheckActuatedEventResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ScadaAutomaticCloseALerts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "scadaAutomaticCloseALerts")
    public JAXBElement<ScadaAutomaticCloseALerts> createScadaAutomaticCloseALerts(ScadaAutomaticCloseALerts value) {
        return new JAXBElement<ScadaAutomaticCloseALerts>(_ScadaAutomaticCloseALerts_QNAME, ScadaAutomaticCloseALerts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnableChecksService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "enableChecksService")
    public JAXBElement<EnableChecksService> createEnableChecksService(EnableChecksService value) {
        return new JAXBElement<EnableChecksService>(_EnableChecksService_QNAME, EnableChecksService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueueSerAutomaticCloseAlerts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "queueSerAutomaticCloseAlerts")
    public JAXBElement<QueueSerAutomaticCloseAlerts> createQueueSerAutomaticCloseAlerts(QueueSerAutomaticCloseAlerts value) {
        return new JAXBElement<QueueSerAutomaticCloseAlerts>(_QueueSerAutomaticCloseAlerts_QNAME, QueueSerAutomaticCloseAlerts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimeToCheckResourceInativeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "timeToCheckResourceInativeResponse")
    public JAXBElement<TimeToCheckResourceInativeResponse> createTimeToCheckResourceInativeResponse(TimeToCheckResourceInativeResponse value) {
        return new JAXBElement<TimeToCheckResourceInativeResponse>(_TimeToCheckResourceInativeResponse_QNAME, TimeToCheckResourceInativeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CfgChiEventTimeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "cfgChiEventTimeResponse")
    public JAXBElement<CfgChiEventTimeResponse> createCfgChiEventTimeResponse(CfgChiEventTimeResponse value) {
        return new JAXBElement<CfgChiEventTimeResponse>(_CfgChiEventTimeResponse_QNAME, CfgChiEventTimeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "user")
    public JAXBElement<String> createUser(String value) {
        return new JAXBElement<String>(_User_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CfgChiEventTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "cfgChiEventTime")
    public JAXBElement<CfgChiEventTime> createCfgChiEventTime(CfgChiEventTime value) {
        return new JAXBElement<CfgChiEventTime>(_CfgChiEventTime_QNAME, CfgChiEventTime.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ScadaAutomaticCloseALertsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "scadaAutomaticCloseALertsResponse")
    public JAXBElement<ScadaAutomaticCloseALertsResponse> createScadaAutomaticCloseALertsResponse(ScadaAutomaticCloseALertsResponse value) {
        return new JAXBElement<ScadaAutomaticCloseALertsResponse>(_ScadaAutomaticCloseALertsResponse_QNAME, ScadaAutomaticCloseALertsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueueSerAutomaticCloseAlertsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "queueSerAutomaticCloseAlertsResponse")
    public JAXBElement<QueueSerAutomaticCloseAlertsResponse> createQueueSerAutomaticCloseAlertsResponse(QueueSerAutomaticCloseAlertsResponse value) {
        return new JAXBElement<QueueSerAutomaticCloseAlertsResponse>(_QueueSerAutomaticCloseAlertsResponse_QNAME, QueueSerAutomaticCloseAlertsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CfgTimePriorizationFSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "cfgTimePriorizationFSResponse")
    public JAXBElement<CfgTimePriorizationFSResponse> createCfgTimePriorizationFSResponse(CfgTimePriorizationFSResponse value) {
        return new JAXBElement<CfgTimePriorizationFSResponse>(_CfgTimePriorizationFSResponse_QNAME, CfgTimePriorizationFSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CfgTimePriorizationFS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "cfgTimePriorizationFS")
    public JAXBElement<CfgTimePriorizationFS> createCfgTimePriorizationFS(CfgTimePriorizationFS value) {
        return new JAXBElement<CfgTimePriorizationFS>(_CfgTimePriorizationFS_QNAME, CfgTimePriorizationFS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnableChecksServiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "enableChecksServiceResponse")
    public JAXBElement<EnableChecksServiceResponse> createEnableChecksServiceResponse(EnableChecksServiceResponse value) {
        return new JAXBElement<EnableChecksServiceResponse>(_EnableChecksServiceResponse_QNAME, EnableChecksServiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventReturnTimerBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "eventReturnTimerBean")
    public JAXBElement<EventReturnTimerBean> createEventReturnTimerBean(EventReturnTimerBean value) {
        return new JAXBElement<EventReturnTimerBean>(_EventReturnTimerBean_QNAME, EventReturnTimerBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimeToCheckActuatedEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "timeToCheckActuatedEvent")
    public JAXBElement<TimeToCheckActuatedEvent> createTimeToCheckActuatedEvent(TimeToCheckActuatedEvent value) {
        return new JAXBElement<TimeToCheckActuatedEvent>(_TimeToCheckActuatedEvent_QNAME, TimeToCheckActuatedEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TimeToCheckResourceInative }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "timeToCheckResourceInative")
    public JAXBElement<TimeToCheckResourceInative> createTimeToCheckResourceInative(TimeToCheckResourceInative value) {
        return new JAXBElement<TimeToCheckResourceInative>(_TimeToCheckResourceInative_QNAME, TimeToCheckResourceInative.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.isd.indra.com/ZeusServicesOPTimerOperationSchedule", name = "password")
    public JAXBElement<String> createPassword(String value) {
        return new JAXBElement<String>(_Password_QNAME, String.class, null, value);
    }

}
